# syntax=docker/dockerfile:1
FROM python:3
ENV PYTHONUNBUFFERED 1

WORKDIR /channels-chat
COPY . /channels-chat/
RUN pip install -r requirements.txt

