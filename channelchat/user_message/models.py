from django.contrib.auth.models import User
from django.db import models

from message.models import DBMessage


class DBUserMessage(models.Model):
    sender = models.ForeignKey(User, related_name='sent_messages', on_delete=models.CASCADE)
    receiver = models.ForeignKey(User, related_name='received_messages', on_delete=models.CASCADE)
    message = models.ForeignKey(DBMessage, related_name="users", on_delete=models.CASCADE)
