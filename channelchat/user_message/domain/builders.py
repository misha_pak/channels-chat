from interfaces.builders import Builder, DestinationObj
from message.domain.builders import MessageReadBuilder
from message.domain.entity import Message
from user.domain.builders import UserReadBuilder
from user.domain.entities import User
from user_message.domain.entities import UserMessage
from user_message.models import DBUserMessage


class UserMessageWriter(Builder[UserMessage, DBUserMessage]):
    def build(self, db_user_message: DBUserMessage) -> None:
        db_user_message.message_id = self._reference_data.message_uuid
        db_user_message.sender_id = self._reference_data.sender_id
        db_user_message.receiver_id = self._reference_data.receiver_id


class DetailedUserMessageReadBuilder(Builder[DBUserMessage, UserMessage]):
    def build(self, user_message: UserMessage) -> None:
        user_message.id = self._reference_data.id

        receiver = User()
        UserReadBuilder(self._reference_data.receiver).build(receiver)
        user_message.receiver = receiver

        sender = User()
        UserReadBuilder(self._reference_data.sender).build(sender)
        user_message.sender = sender

        message = Message()
        MessageReadBuilder(self._reference_data.message).build(message)
        user_message.message = message

