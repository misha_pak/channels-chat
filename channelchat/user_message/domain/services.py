from typing import List

from message.domain.builders import MessageReadBuilder
from message.domain.entity import Message
from message.domain.repository import MessageRepository
from user.domain.entities import User
from user.domain.repository import UserRepository
from user_message.domain.builders import UserMessageWriter, DetailedUserMessageReadBuilder
from user_message.domain.entities import UserMessage, DetailedUserMessage
from user_message.domain.filters import AllMessagesFilter, UserMessagesFilter
from user_message.domain.repository import UserMessageRepository


class UserMessageService:

    @classmethod
    def add_message(cls, sender: User, receiver: User, message: Message):
        UserRepository.get_by_pk(entity=sender, builders=[])
        UserRepository.get_by_pk(entity=receiver, builders=[])

        MessageRepository.get_by_pk(entity=message, builders=[])

        user_message = UserMessage(sender_id=sender.id, receiver_id=receiver.id, message_uuid=message.uuid)

        UserMessageRepository.save(entity=user_message, builders=[UserMessageWriter])

    @classmethod
    def get_all_user_messages(cls, user: User) -> List[DetailedUserMessage]:
        UserRepository.get_by_pk(entity=user, builders=[])

        all_messages = UserMessageRepository.get_all_detailed(filters=[UserMessagesFilter(user=user)],
                                                     builders=[DetailedUserMessageReadBuilder])

        return all_messages
