import uuid as _uuid
from typing import Union, List

from interfaces.builders import Builder
from interfaces.filters import Filter
from interfaces.repository import Repository, Entity
from user_message.domain.entities import UserMessage, DetailedUserMessage
from user_message.models import DBUserMessage


class UserMessageRepository(Repository[UserMessage]):
    @classmethod
    def _get_db_class(cls) -> DBUserMessage:
        return DBUserMessage

    @classmethod
    def _get_entity_class(cls) -> UserMessage:
        return UserMessage

    @classmethod
    def _set_pk(cls, entity: UserMessage, db_instance: DBUserMessage):
        entity.id = db_instance.id

    @classmethod
    def _get_pk(cls, entity: UserMessage) -> Union[int, _uuid.UUID]:
        return entity.id

    @classmethod
    def get_all_detailed(cls, filters: List[Filter], builders: List[Builder]) -> List[Entity]:
        all_db_instances = cls._get_db_class().objects.all()

        for filter in filters:
            all_db_instances = filter.filter(all_db_instances)

        all_entities = []
        for db_instance in all_db_instances:
            entity = DetailedUserMessage()
            for builder in builders:
                builder(db_instance).build(entity)
            cls._set_pk(entity, db_instance)
            all_entities.append(entity)

        return all_entities