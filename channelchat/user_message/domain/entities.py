import uuid as _uuid
from dataclasses import dataclass

from message.domain.entity import Message
from user.domain.entities import User


@dataclass
class UserMessage:
    sender_id: int = None
    receiver_id: int = None
    message_uuid: _uuid.UUID = None
    id: int = None


@dataclass
class DetailedUserMessage:
    sender: User = None
    receiver: User = None
    message: Message = None
    id: int = None
