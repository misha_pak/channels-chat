from typing import Union, List
from django.db.models import Q
from django.db.models import QuerySet

from interfaces.filters import Filter
from user.domain.entities import User


class AllMessagesFilter(Filter):
    def __init__(self, user: User):
        self._user = user

    def filter(self, queryset: Union[QuerySet, List['Model']]) -> Union[QuerySet, List['Model']]:
        return queryset.filter(Q(users__sender_id=self._user.id) | Q(users__receiver_id=self._user.id))

class UserMessagesFilter(Filter):
    def __init__(self, user: User):
        self._user = user

    def filter(self, queryset: Union[QuerySet, List['Model']]) -> Union[QuerySet, List['Model']]:
        return queryset.filter(Q(sender_id=self._user.id) | Q(receiver_id=self._user.id))
