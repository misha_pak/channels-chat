from rest_framework import serializers
from rest_framework_dataclasses.serializers import DataclassSerializer

from user.domain.entities import User
from user_message.domain.entities import DetailedUserMessage


class ReceiverIDSerializer(DataclassSerializer):
    receiver_id = serializers.IntegerField(source='id')

    class Meta:
        dataclass = User
        fields = ['receiver_id']


class SenderIDSerializer(DataclassSerializer):
    sender_id = serializers.IntegerField(source='id')

    class Meta:
        dataclass = User
        fields = ['sender_id']


class DetailedUserMessageSerializer(DataclassSerializer):
    class Meta:
        dataclass = DetailedUserMessage
