from rest_framework import viewsets, permissions
from rest_framework.response import Response

from message.serializers import MessageUUIDSerializer
from user.serializers import UserIDSerializer
from user_message.domain.services import UserMessageService
from user_message.serializers import ReceiverIDSerializer, SenderIDSerializer


class UserMessageViewSet(viewsets.ViewSet):
    permission_classes = [permissions.IsAuthenticated, ]

    def create(self, request):
        sender_ser = SenderIDSerializer(data={'sender_id':request.user.id})
        sender_ser.is_valid(raise_exception=True)
        sender = sender_ser.validated_data

        receiver_ser = ReceiverIDSerializer(data=request.data)
        receiver_ser.is_valid(raise_exception=True)
        receiver = receiver_ser.validated_data

        message_ser = MessageUUIDSerializer(data=request.data)
        message_ser.is_valid(raise_exception=True)
        message = message_ser.validated_data


        UserMessageService.add_message(sender=sender, receiver=receiver, message=message)

        return Response('User Message Created')
