from rest_framework.routers import DefaultRouter

from user_message.api import UserMessageViewSet

router = DefaultRouter()
router.register(r'user_message', UserMessageViewSet, basename='user_message')

user_message_urlpattern = router.urls
