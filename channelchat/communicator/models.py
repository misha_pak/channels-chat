from django.contrib.auth.models import User
from django.db import models

from accounts.models import ProfilePermissions


class Group(models.Model):
    name = models.CharField(max_length=64, unique=True)
    permissions = models.OneToOneField(ProfilePermissions, related_name='group', on_delete=models.SET_NULL, null=True)

    @property
    def last_message(self):
        return self.messages.latest('timestamp')

    def __str__(self):
        return self.name


class GroupProfile(models.Model):
    group = models.OneToOneField(Group, related_name='profile', on_delete=models.CASCADE)
    photo = models.ImageField(null=False, upload_to='profile_images', default="profile_images/default_group.png")


class GroupMessage(models.Model):
    group = models.ForeignKey(Group, related_name='messages', on_delete=models.SET_NULL, null=True)
    sender = models.ForeignKey(User, related_name='group_messages', on_delete=models.SET_NULL, null=True)
    message = models.CharField(max_length=1024)
    timestamp = models.DateTimeField(auto_now_add=True)


class Message(models.Model):
    pass