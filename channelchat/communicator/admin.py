from django.contrib import admin

# Register your models here.
from .models import Group, GroupProfile, GroupMessage


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    extra = 0


@admin.register(GroupProfile)
class GroupProfileAdmin(admin.ModelAdmin):
    extra = 0

class GroupProfileInline(admin.StackedInline):
    model = GroupProfile


@admin.register(GroupMessage)
class GroupMessageAdmin(admin.ModelAdmin):
    list_display = ('group', 'message', 'sender', 'timestamp')
    extra = 0
