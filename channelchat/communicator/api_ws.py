from communicator.interface.generic_consumer import GenericWebsocketView, Response, Request
from user_message.domain.services import UserMessageService
from user_message.serializers import DetailedUserMessageSerializer


class TestAPI(GenericWebsocketView):
    def get(self, request: Request) -> Response:
        all_msg = UserMessageService.get_all_user_messages(user=request.user)

        return Response(DetailedUserMessageSerializer(all_msg, many=True).data)
