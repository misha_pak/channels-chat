from rest_framework import generics, permissions, viewsets
from rest_framework.response import Response

from .serializers import MessagePayloadSerializer, MessageSerializer, ProfilePhotoSerializer, GroupSerializer, \
    GroupIDSerializer, GroupMessageSerializer
from .services.ChatManager import ChatManager
from accounts.models import Profile
from accounts.serializers import UserSerializer

from .services.group import GroupRepository
from accounts.services.account import AccountRepository


class ProfilePictureAPI(generics.GenericAPIView):
    def post(self, request):
        ser = ProfilePhotoSerializer(data=request.data)
        ser.is_valid(raise_exception=True)

        user_profile = getattr(request.user, 'profile', None)

        if user_profile is None:
            Profile.objects.create(owner=request.user)
            user_profile = getattr(request.user, 'profile', None)

        user_profile.photo = ser.validated_data['photo']
        user_profile.save()

        return Response('upl')


class UsersViewset(viewsets.GenericViewSet):
    permission_classes = (permissions.IsAuthenticated,)

    def retrieve(self, request, pk=None):
        manager = ChatManager(request.user)

        history = manager.get_history(receiver_id=pk)

        return Response(MessageSerializer(history, many=True).data)


class Connections(generics.GenericAPIView):
    def get(self, request):
        manager = ChatManager(request.user)
        return Response(UserSerializer(manager.get_receivers(), many=True).data)


class MessageViewset(viewsets.GenericViewSet):
    permission_classes = (permissions.IsAuthenticated,)

    def create(self, request):
        ser = MessagePayloadSerializer(data=request.data)
        ser.is_valid(raise_exception=True)

        manager = ChatManager(request.user)

        message = manager.send_message(message=ser.validated_data['message'],
                                       receiver_id=ser.validated_data['receiver_id'])

        return Response(MessageSerializer(message).data)


class GroupMessagesAPI(generics.GenericAPIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request):
        ser = GroupIDSerializer(data=request.GET)
        ser.is_valid(raise_exception=True)

        account = AccountRepository.wrap(request.user)

        group = GroupRepository.get(ser.validated_data['group_id'])

        messages = group.get_messages(account.instance)

        return Response(GroupMessageSerializer(messages, many=True).data)


class GroupAPI(generics.GenericAPIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request):
        account = AccountRepository.wrap(request.user)

        groups = GroupRepository.get_all(account.permissions)

        ser = GroupSerializer(groups, many=True).data
        return Response(ser)
