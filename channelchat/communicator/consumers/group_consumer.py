import json

from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncWebsocketConsumer
from ..serializers import MessageSerializer, GroupMessageSerializer
from ..services.ChatManager import ChatManager
from ..services.group import GroupRepository


class GroupConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.group_id = self.scope['url_route']['kwargs']['group_id']
        self.room_group_name = f"group_{self.group_id}"

        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    @database_sync_to_async
    def save_message(self, message: str):
        group = GroupRepository.get(id=self.group_id)

        message = group.send_message(message, self.scope['user'])
        return GroupMessageSerializer(message).data

    @database_sync_to_async
    def get_history(self):
        group = GroupRepository.get(id=self.group_id)
        history = group.get_messages(self.scope['user'])

        return GroupMessageSerializer(history, many=True).data

    @database_sync_to_async
    def get_last_msg(self):
        group = GroupRepository.get(id=self.group_id)
        last_msg = group.get_last_message(self.scope['user'])

        return GroupMessageSerializer(last_msg).data

    @database_sync_to_async
    def get_history_with(self, user=""):
        manager = ChatManager(self.scope['user'])

        history = manager.get_history(receiver_id=user)

        return MessageSerializer(history, many=True).data

    async def receive(self, text_data):

        json_data = json.loads(text_data)

        if 'command' in json_data:
            print(json_data['command'])
            if json_data['command'] == 'get_history_with':
                history = await self.get_history_with(json_data['payload']['user'])
                await self.send_message({"type": "history", "payload": history})
            if json_data['command'] == 'get_last_msg':
                last_msg = await self.get_last_msg()
                await self.send_message({"type": "last_msg", "payload": last_msg})
            if json_data['command'] == 'get_history':
                history = await self.get_history()
                await self.send_message({"type": "history", "payload": history})

        if 'type' in json_data:
            if 'message' == json_data['type']:
                message = await self.save_message(json_data['message'])
                await self.send_message({"type": "message", "message": message})

    async def send_message(self, message):
        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': message
            }
        )

    async def chat_message(self, event):
        message = event['message']

        await self.send(text_data=json.dumps(message))
