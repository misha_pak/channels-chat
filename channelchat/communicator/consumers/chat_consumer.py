import json

from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncWebsocketConsumer

from accounts.services.account import AccountRepository

from ..serializers import MessageSerializer


class ChatConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.user_id = self.scope['url_route']['kwargs']['user_id']
        self.room_chat_name = f"user_{self.user_id}"

        await self.channel_layer.group_add(
            self.room_chat_name,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(
            self.room_chat_name,
            self.channel_name
        )

    @database_sync_to_async
    def save_message(self, message: str):
        account = AccountRepository.wrap(self.scope['user'])
        receiver = AccountRepository.get_by_id(self.user_id)

        message = account.send_message(message, receiver.instance)
        return MessageSerializer(message).data

    @database_sync_to_async
    def get_history(self):
        account = AccountRepository.wrap(self.scope['user'])

        receiver = AccountRepository.get_by_id(self.user_id)

        history = account.get_messages(receiver.instance)
        return MessageSerializer(history,many=True).data

    async def receive(self, text_data):
        json_data = json.loads(text_data)

        if 'command' in json_data:
            if json_data['command'] == 'get_history':
                history = await self.get_history()
                await self.send_message({"type": "history", "payload": history})

        if 'type' in json_data:
            if 'message' == json_data['type']:
                message = await self.save_message(json_data['message'])
                await self.send_message({"type": "message", "message": message})

    async def send_message(self, message):
        await self.channel_layer.group_send(
            self.room_chat_name,
            {
                'type': 'chat_message',
                'message': message
            }
        )

    async def chat_message(self, event):
        message = event['message']

        await self.send(text_data=json.dumps(message))
