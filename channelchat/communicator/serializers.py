from rest_framework import serializers
from accounts.serializers import UserSerializer


# class ChatMessageSerializer(serializers.Serializer):
#     milestone = MilestoneSerializer(read_only=True)
#     reference = ChatReferenceSerializer(read_only=True)
#     reputation = ReputationUserSerializer(read_only=True, many=True)


class MessageSerializer(serializers.Serializer):
    sender = UserSerializer()
    receiver = UserSerializer()
    message = serializers.CharField()
    timestamp = serializers.DateTimeField()


class MessagePayloadSerializer(serializers.Serializer):
    receiver_id = serializers.IntegerField()
    message = serializers.CharField()


class ProfilePhotoSerializer(serializers.Serializer):
    photo = serializers.ImageField()


class GroupIDSerializer(serializers.Serializer):
    group_id = serializers.IntegerField()


class GroupMessageSerializer(serializers.Serializer):
    sender = UserSerializer()
    message = serializers.CharField()
    timestamp = serializers.DateTimeField()


class GroupSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField()
    profile = ProfilePhotoSerializer()
    last_message = GroupMessageSerializer()

class MessageSerializer(serializers.Serializer):
    sender = UserSerializer()
    message = serializers.CharField()
    timestamp = serializers.DateTimeField()