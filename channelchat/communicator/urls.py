from django.urls import path

from .api import Connections, ProfilePictureAPI, GroupAPI, GroupMessagesAPI
from .api_ws import TestAPI

urlpatterns = [
    path('groups/all', GroupAPI.as_view()),
    path('groups/messages', GroupMessagesAPI.as_view()),
    path('connections/messages/', Connections.as_view()),
    path('profile/image', ProfilePictureAPI.as_view()),
]

ws_patterns = [
    path('ws/test', TestAPI.as_asgi()),
]
