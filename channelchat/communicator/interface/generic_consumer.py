import json
from abc import abstractmethod
from dataclasses import dataclass

from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncWebsocketConsumer

from user.domain.entities import User
from user.domain.repository import UserRepository


class Response:
    message: str

    def __init__(self, message: str):
        self.message = message


@dataclass
class Request:
    data: str = None
    stream: bytes = None
    user: User = None


class GenericWebsocketView(AsyncWebsocketConsumer):
    _endpoint: str

    def __init__(self, *args, **kwargs):
        self._endpoint = self.__class__.__name__
        super(GenericWebsocketView, self).__init__(*args, **kwargs)

    @property
    def endpoint(self) -> str:
        return self._endpoint

    async def connect(self):
        await self.channel_layer.group_add(
            self.endpoint,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(
            self.endpoint,
            self.channel_name
        )

    @database_sync_to_async
    def __get_user(self) -> User:
        user = User()
        user.id = self.scope['user'].id

        UserRepository.get_by_pk(entity=user, builders=[])

        return user

    async def receive(self, text_data: str, bytes_data: bytes = None):
        user = await self.__get_user()

        request = Request(data=json.loads(text_data), user=user)

        response = await self.__get(request=request)

        await self.send_message(response.message)

    @database_sync_to_async
    def __get(self, request: Request) -> Response:
        return self.get(request=request)

    @abstractmethod
    def get(self, request: Request) -> Response:
        raise NotImplementedError()

    async def send_message(self, message):
        await self.channel_layer.group_send(
            self.endpoint,
            {
                'type': 'chat_message',
                'message': message
            }
        )

    async def chat_message(self, event):
        message = event['message']

        await self.send(text_data=json.dumps(message))
