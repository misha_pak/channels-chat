'''
   When user enters chat he should see the chats he previously entered
   or should be able to find a new chat (new person)
   prevent people from being able to access other peoples' chats
'''
from django.contrib.auth.models import User
from rest_framework.exceptions import APIException

from communicator.models import Message


class CanNotMessageYourself(APIException):
    status_code = 400
    default_detail = "You cannot message yourself"
    default_code = 'cannot_message_yourself'


class ChatManager:
    user: 'User'

    def __init__(self, user: 'User'):
        self.user = user

    def send_to_group(self, message: str, group_id: int) -> Message:

        pass

    def send_message(self, message: str, receiver_id: int) -> Message:
        receiver = User.objects.filter(id=receiver_id)

        if not receiver:
            raise ValueError("receiver_id doesn't exist")

        receiver = receiver.get()

        if receiver.id == self.user.id:
            raise CanNotMessageYourself()

        return Message.objects.create(message=message, sender=self.user, receiver=receiver)

    def get_history(self, receiver_id: int):
        receiver = User.objects.filter(id=receiver_id)

        if not receiver:
            raise ValueError("receiver_id doesn't exist")

        receiver = receiver.get()

        sent_to = Message.objects.filter(sender=self.user, receiver=receiver)
        received_from = Message.objects.filter(sender=receiver, receiver=self.user)
        return sent_to | received_from

    def get_receivers(self):

        all_receivers = Message.objects.filter(sender=self.user).values_list('receiver', flat=True)

        all_sent_from = Message.objects.filter(receiver=self.user).values_list('sender', flat=True)

        return User.objects.filter(id__in=[*all_receivers, *all_sent_from])
