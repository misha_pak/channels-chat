from typing import Optional, List

from ..models import Group, GroupMessage
from accounts.services.account import AccountRepository


class GroupAlreadyExists(Exception):
    pass


class AccountDoesNotHavePermission(Exception):
    pass


class GroupService:
    instance: Group

    def __init__(self, group: Group):
        self.instance = group

    def _check_permisison(self, user):
        account = AccountRepository.wrap(user)

        has_permissions = account.has_permissions(self.instance.permissions)

        if not has_permissions:
            raise AccountDoesNotHavePermission

    def send_message(self, message, sender: 'User') -> GroupMessage:
        self._check_permisison(sender)

        return GroupMessage.objects.create(group=self.instance, sender=sender, message=message)

    def get_messages(self, user: 'User') -> List[GroupMessage]:
        self._check_permisison(user)

        return GroupMessage.objects.filter(group=self.instance)

    def get_last_message(self, user: 'User') -> List[GroupMessage]:
        self._check_permisison(user)

        return GroupMessage.objects.filter(group=self.instance).latest('timestamp')


class GroupRepository:

    @classmethod
    def create(cls, name: str, permissions: 'ProfilePermissions') -> GroupService:
        group = Group.objects.filter(name=name)

        if group.exists():
            raise GroupAlreadyExists

        group = Group.objects.create(name=name, permissions=permissions)

        return cls.wrap(group)

    @classmethod
    def get(cls, id: int) -> Optional[GroupService]:
        group = None

        try:
            group = Group.objects.get(id=id)
        except Group.DoesNotExist:
            return None

        return cls.wrap(group)

    @classmethod
    def wrap(cls, group: 'Group') -> Optional[GroupService]:
        if group is None:
            return None

        return GroupService(group)

    @classmethod
    def get_all(cls, permissions: 'ProfilePermissions'):

        return Group.objects.filter(permissions__regular_user=permissions.regular_user)
