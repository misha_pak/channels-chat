from django.contrib.auth.models import User
from rest_framework.test import APITestCase
from .models import Group
from .services.group import GroupRepository
from accounts.models import ProfilePermissions

from accounts.services.account import AccountRepository


class GroupsTest(APITestCase):
    fixtures = [
        'user.json',
        'profile.json',
        'permissions.json',
        'group.json',
        'group_message.json',
    ]

    def test_adding_group(self):
        group_instance = Group.objects.filter(name='test_group')

        self.assertFalse(group_instance.exists())

        permissions = ProfilePermissions.objects.create()
        GroupRepository.create('test_group', permissions)

        self.assertTrue(group_instance.exists())

    def test_getting_groups(self):
        user = User.objects.get(username="daniels")

        account = AccountRepository.wrap(user)

        groups = GroupRepository.get_all(permissions=account.permissions)

        self.assertEqual(len(groups), 1)
        self.assertEqual(groups[0].name, 'Default Group')

    def test_getting_group_messages(self):
        user = User.objects.get(username="daniels")

        account = AccountRepository.wrap(user)

        group = GroupRepository.get(1)

        messages = group.get_messages(account.instance)

        self.assertEqual(messages.count(), 1)
        self.assertEqual(messages.first().message, 'Hi everyone')

    def test_sending_group_message(self):
        user = User.objects.get(username="daniels")

        account = AccountRepository.wrap(user)

        group = GroupRepository.get(1)

        messages = group.get_messages(account.instance)

        self.assertEqual(messages.count(), 1)
        self.assertEqual(messages.first().message, 'Hi everyone')

        group.send_message('Just another message', account.instance)

        messages = group.get_messages(account.instance)

        self.assertEqual(messages.count(), 2)
        self.assertEqual(messages.first().message, 'Hi everyone')
        self.assertEqual(messages.last().message, 'Just another message')
