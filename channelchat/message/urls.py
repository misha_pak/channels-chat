from rest_framework.routers import DefaultRouter

from message.api import MessageViewSet

router = DefaultRouter()
router.register(r'message', MessageViewSet, basename='message')

message_urlpattern = router.urls
