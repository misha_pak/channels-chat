from django.contrib.auth.models import User
from django.db import models
import uuid as _uuid


class DBMessage(models.Model):
    uuid = models.UUIDField(primary_key=True, editable=False, default=_uuid.uuid4)
    content = models.CharField(max_length=1024)
    timestamp = models.DateTimeField(auto_now_add=True)
    added_by = models.ForeignKey(User, related_name="all_messages", on_delete=models.SET_NULL, null=True)
