from rest_framework import viewsets, permissions
from rest_framework.response import Response

from message.domain.builders import MessageWriteBuilder
from message.domain.repository import MessageRepository
from message.serializers import MessageSerializer, MessageUUIDSerializer


class MessageViewSet(viewsets.ViewSet):
    permission_classes = [permissions.IsAuthenticated, ]

    def create(self, request):
        message_ser = MessageSerializer(data=request.data)
        message_ser.is_valid(raise_exception=True)
        message = message_ser.validated_data

        message.added_by_id = request.user.id

        MessageRepository.save(entity=message, builders=[MessageWriteBuilder])

        return Response(MessageUUIDSerializer(message).data)
