import uuid as _uuid
from typing import Union

from interfaces.repository import Repository
from message.domain.entity import Message
from message.models import DBMessage


class MessageRepository(Repository[Message]):
    @classmethod
    def _get_db_class(cls) -> DBMessage:
        return DBMessage

    @classmethod
    def _get_entity_class(cls) -> Message:
        return Message

    @classmethod
    def _set_pk(cls, entity: Message, db_instance: 'Model'):
        entity.uuid = db_instance.uuid

    @classmethod
    def _get_pk(cls, entity: Message) -> Union[int, _uuid.UUID]:
        return entity.uuid
