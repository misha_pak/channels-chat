import uuid as _uuid
from datetime import datetime
from dataclasses import dataclass


@dataclass
class Message:
    content: str = None
    timestamp: datetime = None
    added_by_id: int = None
    uuid: _uuid.UUID = None
