from interfaces.builders import Builder
from message.domain.entity import Message
from message.models import DBMessage


class MessageWriteBuilder(Builder[Message, DBMessage]):
    def build(self, db_message: DBMessage) -> None:
        db_message.content = self._reference_data.content
        db_message.added_by_id = self._reference_data.added_by_id
        db_message.timestamp = self._reference_data.timestamp


class MessageReadBuilder(Builder[DBMessage, Message]):
    def build(self, message: Message) -> None:
        message.content = self._reference_data.content
        message.added_by_id = self._reference_data.added_by_id
        message.timestamp = self._reference_data.timestamp
        message.uuid = self._reference_data.uuid