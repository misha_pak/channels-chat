from rest_framework import serializers
from rest_framework_dataclasses.serializers import DataclassSerializer

from message.domain.entity import Message


class MessageUUIDSerializer(DataclassSerializer):
    message_uuid = serializers.UUIDField(source='uuid')

    class Meta:
        dataclass = Message
        fields = ['message_uuid']


class MessageSerializer(DataclassSerializer):
    content = serializers.CharField()

    class Meta:
        dataclass = Message
        fields = ['content']

class MessageDetailSerializer(DataclassSerializer):
    content = serializers.CharField()

    class Meta:
        dataclass = Message
