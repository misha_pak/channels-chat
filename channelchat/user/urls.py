from django.urls import path
from knox import views as knox_views

from user.api import LoginAPI, RegisterAPI, ValidateAPI

user_urlpattern = [
    path('login', LoginAPI.as_view(), name='login'),
    path('logout', knox_views.LogoutView.as_view()),
    path('validate', ValidateAPI.as_view()),
    path('register', RegisterAPI.as_view(), name='register'),

]
