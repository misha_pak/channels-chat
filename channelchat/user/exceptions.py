from rest_framework import status
from rest_framework.exceptions import APIException


class UserAlreadyExists(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = "User already exists"
    default_code = "user_exists"
