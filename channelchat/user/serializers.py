from rest_framework import serializers
from rest_framework_dataclasses.serializers import DataclassSerializer

from user.domain.entities import User


class UserIDSerializer(DataclassSerializer):
    user_id = serializers.IntegerField(source='id')

    class Meta:
        dataclass = User
        fields = ['user_id']


class UserTokenSerializer(DataclassSerializer):
    token = serializers.CharField()

    class Meta:
        dataclass = User
        fields = ['token']


class UserSerializer(DataclassSerializer):
    username = serializers.CharField()
    password = serializers.CharField()

    class Meta:
        dataclass = User
        fields = ['username', 'password']
