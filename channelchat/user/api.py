from django.contrib.auth import authenticate
from rest_framework import generics, permissions
from rest_framework.response import Response

from accounts.exceptions import FailedToAuthenticate
from user.domain.builders import TokenReadBuilder, UserWriteBuilder
from user.domain.filters import UsernameFilter
from user.domain.repository import UserRepository, AuthenticationFailed
from user.exceptions import UserAlreadyExists
from user.serializers import UserSerializer, UserTokenSerializer


class ValidateAPI(generics.GenericAPIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request):
        return Response('Token is valid')


class LoginAPI(generics.GenericAPIView):
    permission_classes = [permissions.AllowAny]

    def post(self, request):
        user_ser = UserSerializer(data=request.data)
        user_ser.is_valid(raise_exception=True)
        user = user_ser.validated_data

        try:
            UserRepository.authenticate(entity=user, builders=[TokenReadBuilder])
        except AuthenticationFailed:
            raise FailedToAuthenticate()

        return Response(UserTokenSerializer(user).data)


class RegisterAPI(generics.GenericAPIView):

    def post(self, request):
        user_ser = UserSerializer(data=request.data)
        user_ser.is_valid(raise_exception=True)
        user = user_ser.validated_data

        existing_users = UserRepository.get_all(filters=[UsernameFilter(user=user)], builders=[])

        if existing_users:
            raise UserAlreadyExists()

        UserRepository.create_user(entity=user, builders=[UserWriteBuilder])
        return Response('User created')
