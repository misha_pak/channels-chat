import uuid as _uuid
from typing import Union, List

from django.contrib.auth import authenticate as _authenticate
from django.contrib.auth.models import User as DBUser

from interfaces.builders import Builder
from interfaces.repository import Repository
from user.domain.entities import User


class AuthenticationFailed(Exception):
    pass


class UserRepository(Repository[User]):
    @classmethod
    def _get_db_class(cls) -> DBUser:
        return DBUser

    @classmethod
    def _get_entity_class(cls) -> User:
        return User

    @classmethod
    def _set_pk(cls, entity: User, db_instance: DBUser):
        entity.id = db_instance.id

    @classmethod
    def _get_pk(cls, entity: User) -> Union[int, _uuid.UUID]:
        return entity.id

    @classmethod
    def create_user(cls, entity: User, builders: List[Builder]):
        db_user = DBUser.objects.create_user(username=entity.username, password=entity.password)

        entity.id = db_user.id

        cls.save(entity=entity, builders=builders)

    @classmethod
    def authenticate(cls, entity: User, builders: List[Builder]):
        db_user = _authenticate(username=entity.username, password=entity.password)

        if not db_user:
            raise AuthenticationFailed()

        entity.id = db_user.id

        cls.get_by_pk(entity=entity, builders=builders)
