from typing import Union, List

from django.db.models import QuerySet

from interfaces.filters import Filter
from user.domain.entities import User


class UsernameFilter(Filter):
    _user: User

    def __init__(self, user: User):
        self._user = user

    def filter(self, queryset: Union[QuerySet, List['Model']]) -> Union[QuerySet, List['Model']]:
        return queryset.filter(username=self._user.username)
