from knox.models import AuthToken

from interfaces.builders import Builder
from user.domain.entities import User
from django.contrib.auth.models import User as DBUser


class TokenReadBuilder(Builder[DBUser, User]):
    def build(self, user: User) -> None:
        user.token = AuthToken.objects.create(self._reference_data)[1]


class UserReadBuilder(Builder[DBUser, User]):
    def build(self, user: User) -> None:
        user.username = self._reference_data.username
        user.id = self._reference_data.id


class UserWriteBuilder(Builder[User, DBUser]):
    def build(self, db_user: DBUser) -> None:
        db_user.username = self._reference_data.username
