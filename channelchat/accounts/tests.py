from rest_framework.reverse import reverse
from rest_framework.test import APITestCase
from django.contrib.auth.models import User


class AccountTestsNoAuth(APITestCase):
    fixtures = [
        'user.json',
        'profile.json',
        'permissions.json'
    ]

    def test_creating_user(self):
        response = self.client.post(reverse('login_or_register'), data={'username': 'random_name'}, format='json')

        self.assertEqual(response.status_code, 200)
        self.assertIn('token', response.json())

        user = User.objects.get(username='random_name')

        self.assertIsNot(user.profile, None)
        self.assertIsNot(user.profile.permissions, None)

    def test_authenticating_user(self):
        response = self.client.post(reverse('login_or_register'), data={'username': 'daniels'}, format='json')

        self.assertEqual(response.status_code, 200)
        self.assertIn('token', response.json())


class AccountTestsAuth(APITestCase):
    fixtures = [
        'user.json',
        'profile.json',
        'permissions.json'
    ]

    def setUp(self):
        response = self.client.post(reverse('login_or_register'), data={'username': 'daniels'}, format='json')
        token = response.json()['token']

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token}')

    def test_setting_profile_image(self):
        daniels = User.objects.get(username='daniels')

        profile = daniels.profile
        profile.photo = None
        profile.save()

        self.assertEqual(daniels.profile.photo, None)

        response = None
        with open('accounts/fixtures/default_profile.jpg', 'rb') as img:
            response = self.client.post(reverse('profile_image'), data={"photo": img})

        self.assertEqual(response.status_code, 200)
        self.assertIsNot(daniels.profile.photo, None)
