from rest_framework import status
from rest_framework.exceptions import APIException


class UserDoesNotExist(APIException):
    status_code = 404
    default_detail = "User you are looking for does not exist"
    default_code = "does_not_exist"


class UserAlreadyExists(APIException):
    status_code = 409
    default_detail = 'User already exists'
    default_code = 'already_exists'


class FailedToAuthenticate(APIException):
    status_code = status.HTTP_401_UNAUTHORIZED
    default_detail = "Could not authenticate user"
    default_code = "auth_failed"