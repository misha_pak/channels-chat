from django.contrib.auth.models import User
from rest_framework import serializers

from .models import Profile


class UsernameSerializer(serializers.Serializer):
    username = serializers.CharField()


class ProfileImageSerializer(serializers.Serializer):
    photo = serializers.ImageField()


class UserSerializer(serializers.ModelSerializer):
    profile = ProfileImageSerializer()

    class Meta:
        model = User
        fields = ('id', 'username', 'is_active', 'profile')
