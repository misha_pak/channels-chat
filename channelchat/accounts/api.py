from rest_framework import generics, permissions
from rest_framework.response import Response
from .services.account import AccountService, AccountRepository, FailedToCreateUser
from .serializers import UsernameSerializer, UserSerializer, ProfileImageSerializer


class ValidateAPI(generics.GenericAPIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request):
        return Response({"detail": "Token is valid"})


class LoginOrRegisterAPI(generics.GenericAPIView):
    permission_classes = [permissions.AllowAny]

    def post(self, request):
        ser = UsernameSerializer(data=request.data)
        ser.is_valid(raise_exception=True)

        account = AccountRepository.get(ser.validated_data['username'])

        try:
            if account is None:
                account = AccountRepository.create(ser.validated_data['username'])
        except FailedToCreateUser:
            return Response('Failed to create user', status=400)

        return Response({'token': account.authenticate()})


class ProfileImageAPI(generics.GenericAPIView):
    permission_classes = [permissions.IsAuthenticated]

    def post(self, request):
        ser = ProfileImageSerializer(data=request.data)
        ser.is_valid(raise_exception=True)

        account = AccountRepository.wrap(request.user)
        try:
            account.set_profile_img(ser.validated_data['photo'])
        except Exception:
            return Response('Failed to set profile image; Possibly account has no linked profile', status=400)

        return Response('Profile image has been set', status=200)
