from django.contrib.auth.models import User
from django.db import models


class ProfilePermissions(models.Model):
    regular_user = models.BooleanField(null=False, default=True)


class Profile(models.Model):
    owner = models.OneToOneField(User, related_name='profile', on_delete=models.SET_NULL, null=True)
    permissions = models.OneToOneField(ProfilePermissions, related_name='profile', on_delete=models.SET_NULL, null=True)
    photo = models.ImageField(null=False, upload_to='profile_images', default="../profile_images/default_profile.jpg")
    timestamp = models.DateTimeField(auto_now_add=True)
