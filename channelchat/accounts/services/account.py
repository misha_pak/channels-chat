from typing import Optional, List

from django.contrib.auth.models import User
from django.core.files.uploadedfile import UploadedFile
from django.db import transaction
from knox.models import AuthToken
from ..models import Profile, ProfilePermissions
from communicator.models import Message


class UserAlreadyExists(Exception):
    pass


class FailedToCreateUser(Exception):
    pass


class AccountService:
    instance: User

    def __init__(self, user: User):
        self.instance = user

    def set_profile_img(self, profile_img: UploadedFile):
        profile = self.instance.profile
        profile.photo = profile_img
        profile.save()

    def authenticate(self):
        return AuthToken.objects.create(self.instance)[1]

    def has_permissions(self, permissions: ProfilePermissions) -> bool:
        return self.permissions.regular_user == permissions.regular_user

    @property
    def permissions(self) -> ProfilePermissions:
        return self.instance.profile.permissions

    def send_message(self, message, receiver: 'User') -> Message:
        return Message.objects.create(sender=self.instance, receiver=receiver, message=message)

    def get_messages(self, receiver: 'User') -> List[Message]:
        return Message.objects.filter(receiver=receiver)


class AccountRepository:
    @classmethod
    def get(cls, username) -> Optional[AccountService]:
        user_instance = None
        try:
            user_instance = User.objects.get(username=username)
        except User.DoesNotExist:
            return None

        return cls.wrap(user_instance)

    @classmethod
    def get_by_id(cls, id: int) -> Optional[AccountService]:
        user_instance = None
        try:
            user_instance = User.objects.get(id=id)
        except User.DoesNotExist:
            return None

        return cls.wrap(user_instance)

    @classmethod
    def create(cls, username) -> AccountService:
        user = cls.get(username)

        if user is not None:
            raise UserAlreadyExists

        user = None

        with transaction.atomic():
            user = User.objects.create_user(username=username)
            permissions = ProfilePermissions.objects.create()
            Profile.objects.create(owner=user, permissions=permissions)

        if user is None:
            raise FailedToCreateUser

        return cls.wrap(user)

    @classmethod
    def wrap(cls, user: 'User'):
        return AccountService(user)
