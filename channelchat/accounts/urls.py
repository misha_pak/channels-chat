from django.urls import path
from .api import \
    ValidateAPI, \
    LoginOrRegisterAPI, \
    ProfileImageAPI
from knox import views as knox_views

urlpatterns = [
    path('api/auth/login_or_register', LoginOrRegisterAPI.as_view(), name='login_or_register'),
    path('api/profile/image', ProfileImageAPI.as_view(), name="profile_image"),
    path('api/auth/validate', ValidateAPI.as_view()),
    path('api/auth/logout', knox_views.LogoutView.as_view()),
]
