import abc
from typing import TypeVar, Generic

DestinationObj = TypeVar("DestinationObj")
ReferenceData = TypeVar("ReferenceData")


class Builder(Generic[DestinationObj, ReferenceData], abc.ABC):
    _reference_data: ReferenceData

    def __init__(self, reference_data: ReferenceData):
        self._reference_data = reference_data

    @abc.abstractmethod
    def build(self, destination_obj: DestinationObj) -> None:
        """
        Does not return anything! Rewrites destination_obj instead;
        If nested objects were passed, update nested id/uuid
        """
        raise NotImplementedError()
