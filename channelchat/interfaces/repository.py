import abc
from typing import Generic, TypeVar, List, Union

import uuid as _uuid

from .builders import Builder
from .filters import Filter

Entity = TypeVar("Entity")


class Repository(Generic[Entity], abc.ABC):
    @classmethod
    @abc.abstractmethod
    def _get_db_class(cls) -> "Model":
        raise NotImplementedError()

    @classmethod
    @abc.abstractmethod
    def _get_entity_class(cls) -> Entity:
        raise NotImplementedError()

    @classmethod
    @abc.abstractmethod
    def _set_pk(cls, entity: Entity, db_instance: 'Model'):
        raise NotImplementedError()

    @classmethod
    @abc.abstractmethod
    def _get_pk(cls, entity: Entity) -> Union[int, _uuid.UUID]:
        raise NotImplementedError()

    @classmethod
    def _get_instance(cls, entity: Entity) -> "Model":
        return cls._get_db_class().objects.get(pk=cls._get_pk(entity=entity))

    @classmethod
    def save(cls, entity: Entity, builders: List[Builder]) -> None:
        db_instance = cls._get_db_class()()

        if cls._get_pk(entity=entity) is not None:
            db_instance = cls._get_instance(entity=entity)

        for builder in builders:
            builder(entity).build(db_instance)

        db_instance.save()
        cls._set_pk(entity=entity, db_instance=db_instance)

    @classmethod
    def get_all(cls, filters: List[Filter], builders: List[Builder]) -> List[Entity]:
        all_db_instances = cls._get_db_class().objects.all()

        for filter in filters:
            all_db_instances = filter.filter(all_db_instances)

        all_entities = []
        for db_instance in all_db_instances:
            entity = cls._get_entity_class()()
            for builder in builders:
                builder(db_instance).build(entity)
            cls._set_pk(entity, db_instance)
            all_entities.append(entity)

        return all_entities

    @classmethod
    def get_by_pk(cls, entity: Entity, builders: List[Builder]) -> None:
        db_instance = cls._get_instance(entity)

        for builder in builders:
            builder(db_instance).build(entity)

        cls._set_pk(entity=entity, db_instance=db_instance)

    @classmethod
    def delete(cls, entity: Entity):
        db_instance = cls._get_instance(entity=entity)

        db_instance.delete()
