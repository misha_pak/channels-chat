from typing import List, Union

from django.db.models import QuerySet


class Filter:

    def filter(self, queryset: Union[QuerySet, List['Model']]) -> Union[QuerySet, List['Model']]:
        raise NotImplementedError()
