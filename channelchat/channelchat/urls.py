from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from communicator.urls import ws_patterns
from message.urls import message_urlpattern
from user.urls import user_urlpattern
from user_message.urls import user_message_urlpattern

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(message_urlpattern)),
    path('api/', include(user_message_urlpattern)),
    path('auth/', include(user_urlpattern)),
]

websocket_urlpatterns = [
    *ws_patterns
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
