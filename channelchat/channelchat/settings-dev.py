from .settings import *

ALLOWED_HOSTS = ['*']

ENDPOINT_HOST = "http://localhost:8000"
FRONTEND_HOST = "http://localhost:8080/static"

INSTALLED_APPS += ['corsheaders']

MIDDLEWARE = ['corsheaders.middleware.CorsMiddleware'] + MIDDLEWARE

DEBUG = True

SECURE_SSL_REDIRECT = None

CORS_ALLOW_CREDENTIALS = True
CORS_ORIGIN_WHITELIST = [
    'http://localhost:8080',
]  # If this is used, then not need to use `CORS_ORIGIN_ALLOW_ALL = True`
CORS_ORIGIN_REGEX_WHITELIST = [
    'http://localhost:8080',
]

STATIC_ROOT = BASE_DIR / 'static'

STATIC_URL = "/static/"
MEDIA_URL = "/media/"

STATICFILES_DIRS = [
    BASE_DIR / "frontend/static"
]


