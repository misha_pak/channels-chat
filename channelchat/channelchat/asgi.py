import os

from channels.auth import AuthMiddlewareStack
from channels.db import database_sync_to_async
from channels.routing import ProtocolTypeRouter, URLRouter
from django.contrib.auth.models import User, AnonymousUser
from django.core.asgi import get_asgi_application

from knox.auth import TokenAuthentication

from channelchat.urls import websocket_urlpatterns

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'channelchat.settings')


@database_sync_to_async
def get_user(user_id):
    try:
        return User.objects.get(id=user_id)
    except User.DoesNotExist:
        return AnonymousUser()


@database_sync_to_async
def validate_token(scope):
    '''
        This doesn't seem like a good way to check token...
    '''
    token = None
    for index, item in enumerate(scope['headers']):
        if item[0] == b'cookie':
            decoded = item[1].decode()
            params = decoded.split(";")

            for param in params:
                if 'Authorization' in param:
                    token = param.split("=")[1]
                    break

    if not token:
        return AnonymousUser

    knoxAuth = TokenAuthentication()
    user, auth_token = knoxAuth.authenticate_credentials(token.encode())

    return user


class KnoxAuthMiddleware:
    """
    Custom middleware (insecure) that takes user IDs from the query string.
    """

    def __init__(self, app):
        self.app = app

    async def __call__(self, scope, receive, send):
        scope['user'] = await validate_token(scope)

        return await self.app(scope, receive, send)


TokenAuthMiddlewareStack = lambda inner: KnoxAuthMiddleware(AuthMiddlewareStack(inner))

application = ProtocolTypeRouter({
    "http": get_asgi_application(),
    "websocket": TokenAuthMiddlewareStack(
        URLRouter(websocket_urlpatterns)
    ),
})
